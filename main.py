# -------------------------------------------------- #
# Author: Daniel Jansse                              #
# Date July 13, 2021                                 #
# Purpose: To handle the GUI, and logic for SAT2A    #
# -------------------------------------------------- #
from tkinter import *
file = 'contacts.txt'


# Declare functions
def update_contacts():
    contacts = read_records(file)
    contacts = format_contacts(contacts)
    contact_list.set(contacts)


def search():  # placeholder
    print('')


def create():
    print(fname.get(), sname.get(), email.get(), phone_num.get(), bday.get(), notes.get())
    f = open(file, 'a')

    f.write(fname.get() + '\n')
    f.write(sname.get() + '\n')
    f.write(email.get() + '\n')
    f.write(phone_num.get() + '\n')
    f.write(bday.get() + '\n')
    f.write(notes.get() + '\n')
    f.write('\n')

    f.close()
    update_contacts()
    return


def create_window():  # placeholder
    window_create = Toplevel(window)
    window_create.title("Create a new contact")
    window_create.geometry("280x200")

    Label(window_create, text="First name: ").grid(row=1, column=1, sticky='w')
    Label(window_create, text="Surname: ").grid(row=2, column=1, sticky='w')
    Label(window_create, text="E-mail: ").grid(row=3, column=1, sticky='w')
    Label(window_create, text="Phone number: ").grid(row=4, column=1, sticky='w')
    Label(window_create, text="Birthday: ").grid(row=5, column=1, sticky='w')
    Label(window_create, text="Notes: ").grid(row=6, column=1, sticky='w')

    Entry(window_create, textvariable=fname).grid(row=1, column=2)
    Entry(window_create, textvariable=sname).grid(row=2, column=2)
    Entry(window_create, textvariable=email).grid(row=3, column=2)
    Entry(window_create, textvariable=phone_num).grid(row=4, column=2)
    Entry(window_create, textvariable=bday).grid(row=5, column=2)
    Entry(window_create, textvariable=notes).grid(row=6, column=2)

    Button(window_create, text="Create Contact", command=create).grid(row=7, column=2, sticky='e')


def edit():  # placeholder
    print('')


def delete():  # placeholder
    f = open(file, 'r')
    content = f.readlines()

    line = 0
    for i in content:
        i = i.rstrip('\n')

        if i == email.get():
            print("found!")
            line -= 2
            break
        line += 1
    f.close()

    count = 0
    while count <= 6:
        content[line + count] = ''
        count += 1

    print(content)
    f = open(file, 'w')
    f.writelines(content)
    f.close()

    update_contacts()
    return


def delete_window():
    window_delete = Toplevel(window)
    window_delete.title("Delete contact")
    window_delete.geometry("280x80")

    Label(window_delete, text="Contact e-mail: ").grid(row=1, column=1)
    Entry(window_delete, textvariable=email).grid(row=1, column=2)

    Button(window_delete, text="Delete", command=delete).grid(row=2, column=2, sticky='e')


def format_contacts(listin):  # formats the contacts for the label in "mainframe"
    string = ""
    count = 0
    for i in listin:
        for ii in i:
            if ii == 'notes':
                pass
            else:
                string += i[ii]
                string += " "
                if ii == 'birthday':
                    string += '\n'

    return string


def read_records(file):  # Reads all contacts then puts them into the 'contacts' list
    f = open(file, 'r')
    content = f.readlines()

    count = 0
    records = []
    for line in content:
        line = line.rstrip('\n')
        if count == 0:
            records.append({})
            records[-1]['name'] = line
            count += 1
        elif count == 1:
            records[-1]['surname'] = line
            count += 1
        elif count == 2:
            records[-1]['Email'] = line
            count += 1
        elif count == 3:
            records[-1]['phone_number'] = line
            count += 1
        elif count == 4:
            records[-1]['birthday'] = line
            count += 1
        elif count == 5:
            records[-1]['notes'] = line
            count += 1
        elif count == 6:
            count = 0
    f.close()

    return records


# Create new window object
window = Tk()

# Declare Variables
search_entry = StringVar()
search_entry.set("Search...")  # tmp

contact_list = StringVar()

fname = StringVar()
sname = StringVar()
email = StringVar()
phone_num = StringVar()
bday = StringVar()
notes = StringVar()


# Widgets
Entry(window, text="", width=38, textvariable=search_entry).grid(column=1, row=1, columnspan=2, sticky='we')
Label(window, textvariable=search_entry).grid(column=1, row=5)

mainframe = Frame(window, highlightbackground="white", highlightthickness=1)
mainframe.grid(column=1, row=2, columnspan=2, rowspan=5, sticky='nesw')
Button(window, text="Previous", width=13).grid(column=1, row=7, sticky="w")
Button(window, text="Next", width=13).grid(column=2, row=7, sticky="e")

Button(window, text="Search", width=13).grid(column=3, row=2)
Button(window, text="Create", command=create_window, width=13).grid(column=3, row=3)
Button(window, text="Edit", width=13).grid(column=3, row=4)
Button(window, text="Delete", width=13, command=delete_window).grid(column=3, row=5)

noteframe = Frame(window, highlightbackground="white", highlightthickness=1, height=150)
noteframe.grid(column=3, row=6, rowspan=1, sticky="we")
Label(mainframe, text="sampletext", textvariable=contact_list, justify=LEFT).grid(column=1, row=1)


update_contacts()


# Enter window's event loop
window.title('Address Book')
window.geometry("600x380")
window.mainloop()
